package conteudo;

import javax.swing.JFormattedTextField;
import javax.swing.JTextField;


public class Pessoa {

    private String nome;
    private String numero;

    public Pessoa(String nome, String numero) {
        this.nome = nome;
        this.numero = numero;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setTelefone(String numero) {
        this.numero = numero;
    }

    public String getTelefone() {
        return this.numero;
    }

    @Override
    public String toString() {
      
        return "Nome: " + this.nome + " - " + "Telefone: " + this.numero;
    }

    @Override
    public boolean equals(Object outraPessoa) {

        if (outraPessoa instanceof Pessoa) {
          
            Pessoa p = (Pessoa) outraPessoa;

            return this.nome.equals(p.getNome()) && this.numero.equals(p.getTelefone());

        }

        return false;
    }

    public void setNome(JTextField jTextFieldNome) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTelefone(JFormattedTextField jFormattedTextFieldnumero) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
